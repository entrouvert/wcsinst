import json
import logging

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from jsonresponse import to_json

from .deploy import DeployInstance

logger = logging.getLogger(__name__)

@csrf_exempt
@to_json('plain')
@require_POST
def create(request):
    data = json.loads(request.body)
    deploy = DeployInstance(**data)
    deploy.make()
    return {}


@csrf_exempt
@to_json('plain')
@require_POST
def update(request, instance):
    print 'updating instance:', instance
    data = json.loads(request.body)
    if data.get('domain') != instance:
        raise Exception('domain mismatch') # -> should remove/add ?
    deploy = DeployInstance(**data)
    deploy.make()
    return {}
