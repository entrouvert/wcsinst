from django.conf.urls import patterns, url, include

urlpatterns = patterns('wcsinst.wcsinstd.views',
    url(r'^create$', 'create'),
    url(r'^(?P<instance>[\w\.-]+)/$', 'update'),
)

