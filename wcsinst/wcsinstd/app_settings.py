from django.conf import settings

URL_TEMPLATE = getattr(settings, 'WCSINST_URL_TEMPLATE', 'https://%(domain)s')
WCS_APP_DIR = getattr(settings, 'WCSINST_WCS_APP_DIR', None)
WCSCTL_SCRIPT = getattr(settings, 'WCSINST_WCSCTL_SCRIPT', 'wcsctl')
