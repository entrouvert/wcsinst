# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'WcsInstance.saml2_use_role'
        db.delete_column(u'wcsinst_wcsinstance', 'saml2_use_role')

        # Deleting field 'WcsInstance.saml2_role_prefix'
        db.delete_column(u'wcsinst_wcsinstance', 'saml2_role_prefix')


    def backwards(self, orm):
        # Adding field 'WcsInstance.saml2_use_role'
        db.add_column(u'wcsinst_wcsinstance', 'saml2_use_role',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.saml2_role_prefix'
        db.add_column(u'wcsinst_wcsinstance', 'saml2_role_prefix',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)


    models = {
        u'wcsinst.apisecret': {
            'Meta': {'object_name': 'ApiSecret'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'api_secrets'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.variable': {
            'Meta': {'object_name': 'Variable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'variables'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.wcsinstance': {
            'Meta': {'object_name': 'WcsInstance'},
            'backoffice_feed_url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'clicrdv': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'domain': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'domino': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'drupal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ezldap': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postgresql': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'strongbox': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['wcsinst']