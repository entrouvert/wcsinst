# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'WcsInstance', fields ['domain']
        db.create_unique(u'wcsinst_wcsinstance', ['domain'])

        # Adding unique constraint on 'WcsInstance', fields ['title']
        db.create_unique(u'wcsinst_wcsinstance', ['title'])


    def backwards(self, orm):
        # Removing unique constraint on 'WcsInstance', fields ['title']
        db.delete_unique(u'wcsinst_wcsinstance', ['title'])

        # Removing unique constraint on 'WcsInstance', fields ['domain']
        db.delete_unique(u'wcsinst_wcsinstance', ['domain'])


    models = {
        u'wcsinst.apisecret': {
            'Meta': {'object_name': 'ApiSecret'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'api_secrets'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.variable': {
            'Meta': {'object_name': 'Variable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'variables'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.wcsinstance': {
            'Meta': {'object_name': 'WcsInstance'},
            'backoffice_feed_url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'clicrdv': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'domain': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'domino': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'drupal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ezldap': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postgresql': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'saml2_role_prefix': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'saml2_use_role': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'strongbox': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'})
        }
    }

    complete_apps = ['wcsinst']