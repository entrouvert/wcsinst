# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Variable'
        db.create_table(u'wcsinst_variable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('wcs_instance', self.gf('django.db.models.fields.related.ForeignKey')(related_name='variables', to=orm['wcsinst.WcsInstance'])),
        ))
        db.send_create_signal(u'wcsinst', ['Variable'])

        # Adding model 'ApiSecret'
        db.create_table(u'wcsinst_apisecret', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1024, blank=True)),
            ('wcs_instance', self.gf('django.db.models.fields.related.ForeignKey')(related_name='api_secrets', to=orm['wcsinst.WcsInstance'])),
        ))
        db.send_create_signal(u'wcsinst', ['ApiSecret'])

        # Adding field 'WcsInstance.postgresql'
        db.add_column(u'wcsinst_wcsinstance', 'postgresql',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.saml2_use_role'
        db.add_column(u'wcsinst_wcsinstance', 'saml2_use_role',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.saml2_role_prefix'
        db.add_column(u'wcsinst_wcsinstance', 'saml2_role_prefix',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'WcsInstance.backoffice_feed_url'
        db.add_column(u'wcsinst_wcsinstance', 'backoffice_feed_url',
                      self.gf('django.db.models.fields.URLField')(default='', max_length=128, blank=True),
                      keep_default=False)

        # Adding field 'WcsInstance.drupal'
        db.add_column(u'wcsinst_wcsinstance', 'drupal',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.ezldap'
        db.add_column(u'wcsinst_wcsinstance', 'ezldap',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.strongbox'
        db.add_column(u'wcsinst_wcsinstance', 'strongbox',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.clicrdv'
        db.add_column(u'wcsinst_wcsinstance', 'clicrdv',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'WcsInstance.domino'
        db.add_column(u'wcsinst_wcsinstance', 'domino',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Variable'
        db.delete_table(u'wcsinst_variable')

        # Deleting model 'ApiSecret'
        db.delete_table(u'wcsinst_apisecret')

        # Deleting field 'WcsInstance.postgresql'
        db.delete_column(u'wcsinst_wcsinstance', 'postgresql')

        # Deleting field 'WcsInstance.saml2_use_role'
        db.delete_column(u'wcsinst_wcsinstance', 'saml2_use_role')

        # Deleting field 'WcsInstance.saml2_role_prefix'
        db.delete_column(u'wcsinst_wcsinstance', 'saml2_role_prefix')

        # Deleting field 'WcsInstance.backoffice_feed_url'
        db.delete_column(u'wcsinst_wcsinstance', 'backoffice_feed_url')

        # Deleting field 'WcsInstance.drupal'
        db.delete_column(u'wcsinst_wcsinstance', 'drupal')

        # Deleting field 'WcsInstance.ezldap'
        db.delete_column(u'wcsinst_wcsinstance', 'ezldap')

        # Deleting field 'WcsInstance.strongbox'
        db.delete_column(u'wcsinst_wcsinstance', 'strongbox')

        # Deleting field 'WcsInstance.clicrdv'
        db.delete_column(u'wcsinst_wcsinstance', 'clicrdv')

        # Deleting field 'WcsInstance.domino'
        db.delete_column(u'wcsinst_wcsinstance', 'domino')


    models = {
        u'wcsinst.apisecret': {
            'Meta': {'object_name': 'ApiSecret'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'api_secrets'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.variable': {
            'Meta': {'object_name': 'Variable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024', 'blank': 'True'}),
            'wcs_instance': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'variables'", 'to': u"orm['wcsinst.WcsInstance']"})
        },
        u'wcsinst.wcsinstance': {
            'Meta': {'object_name': 'WcsInstance'},
            'backoffice_feed_url': ('django.db.models.fields.URLField', [], {'max_length': '128', 'blank': 'True'}),
            'clicrdv': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'domino': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'drupal': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ezldap': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'postgresql': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'saml2_role_prefix': ('django.db.models.fields.CharField', [], {'max_length': '128', 'blank': 'True'}),
            'saml2_use_role': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'strongbox': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['wcsinst']