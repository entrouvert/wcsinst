# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'WcsInstance'
        db.create_table(u'wcsinst_wcsinstance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('domain', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'wcsinst', ['WcsInstance'])


    def backwards(self, orm):
        # Deleting model 'WcsInstance'
        db.delete_table(u'wcsinst_wcsinstance')


    models = {
        u'wcsinst.wcsinstance': {
            'Meta': {'object_name': 'WcsInstance'},
            'domain': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['wcsinst']