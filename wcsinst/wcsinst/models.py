import json
import logging
import urllib2

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

logger = logging.getLogger(__name__)

class WcsInstance(models.Model):
    title = models.CharField(max_length=50, unique=True)
    domain = models.CharField(max_length=100, unique=True)

    # site-options.cfg options
    postgresql = models.BooleanField(verbose_name=_('postgresql'),
            blank=True)
    backoffice_feed_url = models.URLField(verbose_name=_('backoffice feed url'),
            blank=True, max_length=128)
    drupal = models.BooleanField(verbose_name=_('drupal'),
            blank=True)
    ezldap = models.BooleanField(verbose_name=_('ezldap'),
            blank=True)
    strongbox = models.BooleanField(verbose_name=_('strongbox'),
            blank=True)
    clicrdv = models.BooleanField(verbose_name=_('clicrdv'),
            blank=True)
    domino = models.BooleanField(verbose_name=_('domino'),
            blank=True)


    def __unicode__(self):
        return '%s (%s)' % (self.title, self.domain)

    def site_options_cfg(self):
        d = {
                'postgresql': self.postgresql,
                'backoffice_feed_url': self.backoffice_feed_url,
                'drupal': self.drupal,
                'ezldap': self.ezldap,
                'strongbox': self.strongbox,
                'clicrdv': self.clicrdv,
                'domino': self.domino,
        }
        d['api_secrets'] = dict((kv.key, kv.value) for kv in self.api_secrets.all())
        d['variables'] = dict((kv.key, kv.value) for kv in self.variables.all())
        return d

    def to_json(self):
        return {
                'title': self.title,
                'domain': self.domain,
                'site_options_cfg': self.site_options_cfg(),
        }

    def notify(self, created=True):
        # notify wcsinstd
        if not settings.WCSINSTD_URL:
            return
        if created:
            url = settings.WCSINSTD_URL + 'wcsinstd/create'
        else:
            url = settings.WCSINSTD_URL + 'wcsinstd/%s/' % self.domain
        post_data = json.dumps(self.to_json())
        request = urllib2.Request(url)
        request.add_header('Accept', 'application/json')
        request.add_header('Content-Type', 'application/json;charset=UTF-8')
        request.add_data(post_data)
        try:
            p = urllib2.urlopen(request)
        except urllib2.HTTPError as e:
            logger.error('wcsinstd HTTP error (%s)', str(e))
            print e.read()
        except urllib2.URLError as e:
            print e
            logger.error('wcsinstd URL error (%s)', str(e))
        else:
            out_data = p.read()
            p.close()

    def save(self, *args, **kwargs):
        created = (self.id is None)
        super(WcsInstance, self).save(*args, **kwargs)

    class Meta:
        verbose_name = _('wcs instance')
        verbose_name_plural = _('wcs instances')


class KeyValue(models.Model):
    key = models.CharField(max_length=128)
    value = models.CharField(max_length=1024, blank=True)

    class Meta:
        abstract = True


class Variable(KeyValue):
    wcs_instance = models.ForeignKey(WcsInstance, related_name='variables')

    class Meta:
        verbose_name = _('variable')
        verbose_name_plural = _('variables')


class ApiSecret(KeyValue):
    wcs_instance = models.ForeignKey(WcsInstance, related_name='api_secrets')

    class Meta:
        verbose_name = _('api secret')
        verbose_name_plural = _('api secrets')


