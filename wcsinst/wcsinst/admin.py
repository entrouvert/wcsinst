from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from models import WcsInstance, ApiSecret, Variable

class ApiSecretsInline(admin.TabularInline):
    model = ApiSecret
    verbose_name = _('API secret')
    verbose_name_plural = _('API secrets')

class VariablesInline(admin.TabularInline):
    model = Variable
    verbose_name = _('variable')
    verbose_name_plural = _('variables')

class WcsInstanceAdmin(admin.ModelAdmin):
    prepopulated_fields = {'domain': ('title',)}
    fieldsets = (
        (None, {'fields': ('title', 'domain'),}),
        ('site-options.cfg',
            {'fields': ('postgresql', 'backoffice_feed_url' )}
        ),
        ('site-options.cfg au-quotidien',
            {'fields': ('drupal', 'ezldap', 'strongbox', 'clicrdv', 'domino' )}
        ),
    )
    readonly_fields = ('domain',)
    inlines = [VariablesInline, ApiSecretsInline]
    save_as = True

    def save_related(self, request, form, formsets, change):
        super(WcsInstanceAdmin, self).save_related(request, form, formsets, change)
        form.instance.notify(not change)

    def get_prepopulated_fields(self, request, obj=None):
        if obj:
            return {}
        return self.prepopulated_fields

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = self.readonly_fields
        if not obj:
            return filter(lambda x: x != 'domain', readonly_fields)
        return readonly_fields


admin.site.register(WcsInstance, WcsInstanceAdmin)
