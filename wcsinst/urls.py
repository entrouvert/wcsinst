from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
)

if 'wcsinst.wcsinstd' in settings.INSTALLED_APPS:
    urlpatterns += patterns('',
            (r'^wcsinstd/', include('wcsinst.wcsinstd.urls')))
