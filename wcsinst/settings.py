# Django settings for wcsinst project.

import os
from ConfigParser import SafeConfigParser

# get configuration files from :
# 1. default-settings.ini from source code
# 2. os.environ.get('SETTINGS_INI') if it exists
#    else /etc/wcsinstd/settings.ini
#         and then /etc/wcsinstd/local-settings.ini
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SETTINGS_INI = (os.path.join(BASE_DIR, 'default-settings.ini'),)
if os.environ.get('SETTINGS_INI'):
    SETTINGS_INI += (os.environ.get('SETTINGS_INI'),)
else:
    ETC_DIR = os.path.join('/', 'etc', 'wcsinstd')
    SETTINGS_INI += (
        os.path.join(ETC_DIR, 'settings.ini'),
        os.path.join(ETC_DIR, 'local-settings.ini')
    )

config = SafeConfigParser()
config.read(SETTINGS_INI)


DEBUG = config.getboolean('debug', 'general')
INTERNAL_IPS = tuple(config.get('debug', 'internal_ips').split())
TEMPLATE_DEBUG = config.getboolean('debug', 'template')
ADMINS = tuple(config.items('admins'))
MANAGERS = tuple(config.items('managers'))
SENTRY_DSN = config.get('debug', 'sentry_dsn')
DEBUG_TOOLBAR = config.getboolean('debug', 'toolbar')

DATABASES = {
    'default': {
        'ENGINE': config.get('database', 'engine'),
        'NAME': config.get('database','name'),
        'USER': config.get('database','user'),
        'PASSWORD': config.get('database','password'),
        'HOST': config.get('database','host'),
        'PORT': config.get('database','port'),
    }
}

# Hosts/domain names that are valid for this site; required if DEBUG is False
# See https://docs.djangoproject.com/en/1.5/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
USE_X_FORWARDED_HOST = True

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'Europe/Paris'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'fr-fr'
gettext_noop = lambda s: s
LANGUAGES = (
    ('en', gettext_noop('English')),
    ('fr', gettext_noop('French')),
)

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/var/www/example.com/media/"
MEDIA_ROOT = config.get('dirs','media_root')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://example.com/media/", "http://media.example.com/"
MEDIA_URL = ''

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = config.get('dirs','static_root')

# URL prefix for static files.
# Example: "http://example.com/static/", "http://static.example.com/"
STATIC_URL = '/static/'

# Additional locations of static files
STATICFILES_DIRS = tuple(config.get('dirs','static_dirs').split())

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = config.get('secrets', 'secret_key')

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'wcsinst.wsgi.application'

TEMPLATE_DIRS = tuple(config.get('dirs', 'template_dirs').split())

INSTALLED_APPS = (
    'south',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
)

ROOT_URLCONF = 'wcsinst.urls'

if config.getboolean('cache', 'memcached'):
    CACHES = {
        'default': {
            'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
            'LOCATION': '127.0.0.1:11211',
        },
    }

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s %(name)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S'
            },
        'syslog': {
            'format': '%(levelname)s %(name)s: %(message)s',
            },
        },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class':'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
            },
        'syslog': {
            'level': 'DEBUG',
            'address': '/dev/log',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'syslog',
            },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        '': {
            'handlers': ['mail_admins', 'syslog', 'console'],
            'level': 'INFO',
        }
    }
}

# debug settings
if DEBUG:
    LOGGING['loggers']['']['level'] = 'DEBUG'

# email settings
EMAIL_HOST = config.get('email', 'host')
EMAIL_PORT = config.getint('email', 'port')
EMAIL_HOST_USER = config.get('email', 'user')
EMAIL_HOST_PASSWORD = config.get('email', 'password')
EMAIL_SUBJECT_PREFIX = config.get('email', 'subject_prefix')
EMAIL_USE_TLS = config.getboolean('email', 'use_tls')
SERVER_EMAIL = config.get('email', 'server_email')
DEFAULT_FROM_EMAIL = config.get('email', 'default_from_email')

# wcsinst / wcsintd settings
WCSINSTD_URL = os.environ.get('WCSINSTD_URL')
if WCSINSTD_URL:
    INSTALLED_APPS += ('wcsinst.wcsinst',)
else:
    INSTALLED_APPS += ('wcsinst.wcsinstd',)
WCSINST_URL_TEMPLATE = config.get('wcsinstd', 'url_template')
WCSINST_WCSCTL_SCRIPT = config.get('wcsinstd', 'wcsctl_script')
WCSINST_WCS_APP_DIR = config.get('wcsinstd', 'wcs_app_dir')

# debug toolbar needs more
if DEBUG_TOOLBAR:
    DEBUG_TOOLBAR_CONFIG = {'INTERCEPT_REDIRECTS': False}
    INSTALLED_APPS += ('debug_toolbar',)
    MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware',)

# sentry needs more
if SENTRY_DSN:
    RAVEN_CONFIG = {'dsn': SENTRY_DSN}
    INSTALLED_APPS += ('raven.contrib.django.raven_compat',)
    LOGGING['handlers']['sentry'] = {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            }
    LOGGING['loggers']['']['handlers'].append('sentry')

try:
    from local_settings import *
except ImportError:
    pass

