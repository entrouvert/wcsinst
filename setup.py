#! /usr/bin/python

from setuptools import setup, find_packages
import os
import subprocess

VERSION='0.1'

def get_version():
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--dirty'], stdout=subprocess.PIPE)
        result = p.communicate()[0]
        return result.split()[0].replace('-','.')
    return VERSION

setup(name='wcsinst',
        version=get_version(),
        license='AGPLv3',
        description='',
        url='https://dev.entrouvert.org/projects/wcsinst/',
        download_url='http://repos.entrouvert.org/wcsinst.git/',
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages(os.path.dirname(__file__) or '.'),
        scripts=['manage.py'],
        include_package_data = True,
        install_requires=[
            'django >= 1.5.1, < 1.6',
            'django-jsonresponse==0.5',
        ],
)
