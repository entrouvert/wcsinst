#!/bin/bash -e
pip install --upgrade pip
pip install --upgrade pylint
pip install --upgrade -v -r requirements.txt
echo Nothing to test for now
(pylint -f parseable --rcfile /var/lib/jenkins/pylint.django.rc wcsinst/ | tee pylint.out) || /bin/true
